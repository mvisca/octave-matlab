function  [A,B]=Fc(D)
	
	A=zeros(rows(D),columns(D)-2);
	B=zeros(rows(D),1);
	R=4;N=1;
	

	t=D(:,1); the=D(:,2); T=D(:,3); iu=D(:,4);

	for k=2:(rows(D)-1)								 % Arranca desde k=2 para poder hacer uso del k anterior

		A(k,1)=((1+R*(iu(k))^2)/(1+R))^N;

		A(k,2)=-(the(k+1)-the(k-1))/(t(k+1)-t(k-1));  %Uso de diferenciacion centrada 

		B(k,1)=the(k)-T(k);
	end

	sol=A\B; 

	disp(sprintf('Dth= %f , Tau= %f , con ctes R= %f y N= %f', sol(1), sol(2), R, N)) 