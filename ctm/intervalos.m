%Cargo los datos, ordenados segun el banco y grupos
%el .csv debe de estar delimitado por tabulaciones

D=dlmread('datos.csv','\t');

%El array bancos almacena los intervalos a estudiar con el mismo orden que
% los datos pero de forma descendente
bancos={};

for m=2:columns(D)
    
    %el conjunto de indices esta dado por todas las entradas que son nulas en cada columna.
    % ind2 almacena los intervalos a estudiar a medida q los encuentra.
    ind=find(D(:,m)==0);
    j=1;
    ind2=[];

    for i=1:length(ind)-1
	if((ind(i)+1)!=ind(i+1) && ((-(ind(i)+1)+(ind(i+1)-1))>=480))	%el numero 480 es el equivalante a 8hs de dif. en eventos
	    ind2(j,:)=[ind(i)+1,ind(i+1)-1];				%extraigo los intervalos a estudiar
	    j=j+1;
	end
    end
    
    bancos(m-1)=D(ind2);						%los almaceno en el array

end

save intervalos.txt bancos						%idem en formato .text
