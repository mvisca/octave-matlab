%Funcion G con cambios de variables en R e i.

function [Gx]=G(D, r, dth, tau, N)

	Gx=zeros(rows(D)-2, 1);

	t=D(:,1); the=D(:,2); T=D(:,3); I=((D(:,4)).^2 -1);
	
	for k=2:rows(D)-1
		Gx(k)=((1+r*I(k))^N)*dth -tau*((the(k+1)-the(k-1))/(t(k+1)-t(k-1)))-the(k)+T(k);

	end

