
%Jacobiano de la funcion G


function [Jg]=jacg(D, r, dth, tau, N)

	Jg=zeros(rows(D)-2, 4);

	t=D(:,1); the=D(:,2); T=D(:,3); I=((D(:,4)).^2 -1);

	for k=2:(rows(D)-1)
		
		Jg(k,1)= (N*(1+r*I(k))^(N-1))*I(k)*dth;
		Jg(k,2)=(1+r*I(k))^N;
		Jg(k,3)=-((the(k+1)-the(k-1))/(t(k+1)-t(k-1)));
		Jg(k,4)=log(1+r*I(k))*(1+r*I(k))^N;

	end
