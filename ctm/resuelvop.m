
function [x,p]=resuelvop(A,b)
n=length(b);
% Escalerizo

A1=[A,b];
p=[1:n]';
for k=1:n
	if A1(p(k),k)==0					% evaluar la matriz en la fila p(k) es necesario, sino no respeta el orden y hace cualquiera
		[mc,imc]=max(abs(A1(k:n,k)));
		imc=imc+k-1;
		aux=p(k);
		p(k)=imc;
		p(imc,k)=aux;
	end
	for i=p(k)+1:n 						% la i debe de iniciar en p(k), por la misma razon, sino no escaleriza como debe

		if p(k)==i 						% con esta sentencia aseguro los valores de la fila, escalerizo todo, menos la esta fila, es decir los valores por encima y por debajo
			A1(p(k),:)=A1(p(k),:);
		 
		else
			 A1(i,:)=A1(i,:)-A1(i,k)/A1(p(k),k)*A1(p(k),:);
		end	
			
	end
end

% Sustituyo hacia atras

b1=A1(:,n+1);
x=zeros(n,1);
x(p(n))=b1(n)/A1(p(n),n);
for k=p(n)-1:-1:1 											% el inicio de k tambien depende de p(k) recorrido hacia atras
	x(p(k))=(b1(p(k))-A1(p(k),k+1:n)*x(k+1:n))/A1(p(k),k);  % y poder ir remplazando correctamente, y que exista la correlacion 
															% entre la sustitucion y las permutas q contiene p
end

%				PD: a huevo, con hoja y lapiz se entiende el razonamiento...
% 				    Por otra parte la los valores de las incognitas son coherentes con los indices de p...