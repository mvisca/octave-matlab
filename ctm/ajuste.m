% Importo los datos(temperaturas, corrientes, intervalos):
% Datos=xlsread('enero_2016.xlsx','temperaturas'); I=xlsread('enero_2016.xlsx','corriente'); load intervalos.txt;

% Hago el ajuste para cada uno de los bancos y sus fases respectivas.

function ajuste(Datos,I,banco,fase,int)
    
    if(banco==1)
	tmp=find(Datos(:,1)>=int(1,1) & int(1,2)>=Datos(:,1));
	Dts=[Datos(tmp,[2 3 4 11]) I(tmp,2)];
	disp('en1')
	if(strcmp(fase,'r')==1)
	    Toil=Dts(:,1); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)
	
	elseif(strcmp(fase,'s')==1)
	    Toil=Dts(:,2); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)

	elseif(strcmp(fase,'t')==1)
	    Toil=Dts(:,3); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)

	end


    elseif(banco==3)	
	tmp=find(Datos(:,1)>=int(1,1) & int(1,2)>=Datos(:,1));
	Dts=[Datos(tmp,[5 6 7 11]) I(tmp,3)];
	disp('en2');
	if(strcmp(fase,'r')==1)
	    Toil=Dts(:,1); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)
	
	elseif(strcmp(fase,'s')==1)
	    Toil=Dts(:,2); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)

	elseif(strcmp(fase,'t')==1)
	    Toil=Dts(:,3); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)

	end

    elseif(banco==5)
	tmp=find(Datos(:,1)>=int(1,1) & int(1,2)>=Datos(:,1));
	Dts=[Datos(tmp,[8 9 10 11]) I(tmp,4)];
	disp('en3')
 	if(strcmp(fase,'r')==1)
	    Toil=Dts(:,1); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)
	
	elseif(strcmp(fase,'s')==1)
	    Toil=Dts(:,2); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)

	elseif(strcmp(fase,'t')==1)
	    Toil=Dts(:,3); Tamb=Dts(:,4); Inten=Dts(:,5);
	    ajusteLineal(Toil,Tamb,Inten)
	end

    end

end
