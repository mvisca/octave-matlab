% Ajuste por minimos cuadrados de Delta Theta y Tau, aprox. de derivada hacia adelante.
% Ecuaciones lineales: A*DeltaTh - B*Tau = Toil-Tamb.

% Mario Visca.


function  ajusteLineal(Toil,Tamb,Inten)
	
	A=zeros(rows(Toil),2);
	B=zeros(rows(Toil),1);
	residuo=[];
	R=4; N=1;
	
	for k=1:(rows(Toil)-1)

	    A(k,1)=((1+R*(Inten(k))^2)/(1+R))^N;

	    A(k,2)=-(Toil(k+1)-Toil(k))/(15);

	    B(k,1)=Toil(k)-Tamb(k);
	end

	sol=A\B; 

	disp(sprintf('Dth= %f , Tau= %f , con ctes R= %f y N= %f', sol(1), sol(2), R, N)) 

% Calculo del vector error y calculo del residuo promedio.

	for k=1:(rows(Toil)-1)

	    residuo(k)=[((1+R*(Inten(k))^2)/(1+R))^N]*sol(1)+[-(Toil(k+1)-Toil(k))/(15)]*sol(2)-Toil(k)+Tamb(k);

	end

	Residuo_Promedio=norm(residuo,inf)/(length(residuo)) 

end
